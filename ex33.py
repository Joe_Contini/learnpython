def wloop(runs,inc):
	i = 0
	numbers = []

	while i < runs:
		print "At the top i is %d" % i
		numbers.append(i)

		i = i + inc
		print "Numbers now:", numbers
		print "At the bottom i is %d" % i

	print "The numbers: "

	for num in numbers:
		print num


def floop(runs):
	i = 0
	numbers = []
	
	for i in range(0,runs):
		print "At the top i is %d" % i
		numbers.append(i)

		print "Numbers now:", numbers
		print "At the bottom i is %d" % i


floop(6)