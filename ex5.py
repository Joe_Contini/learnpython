name = 'Joe Contini'
age = 26 # not a lie
height = 67 # inches
weight = 140 # lbs
eyes = 'Brown'
teeth = 'White'
hair = 'Black'

print "Let's talk about %r" % name
print "He's %r inches tall." % height
print "Or %d centimeters tall." % (height * 2.54)
print "He's %r pounds heavy." % weight
print "Actually that's not too heavy."
print "He's got %r eyes and %r hair." % (eyes, hair)
print "His teeth are usualy %r depending on the coffee." % teeth

# this line is trycky, try to get it exactly right
print "If I add %r, %r, and %r I get %r." % (age, height, weight, age + height + weight)