class Song(object):

	def __init__(self, lyrics):
		self.lyrics = lyrics

	def sing_me_a_song(self):
		print '-' * 15
		for line in self.lyrics:
			print line
		print '-' * 15

happy_bday_lines = ["Happy birthday to you",
	"I don't want to get sued",
	"So I'll stop right there"]

bulls_on_parade_lines = ["They rally around the family",
	"With pockets full of shells"]

Happy = Song(happy_bday_lines)
Bulls = Song(bulls_on_parade_lines)

Happy.sing_me_a_song()
Bulls.sing_me_a_song()