<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.10: http://docutils.sourceforge.net/" />
<title>Exercise 42: Is-A, Has-A, Objects, and Classes</title>

  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="stylesheets/foundation.min.css">
  <link rel="stylesheet" href="stylesheets/pygments.css">
  <link rel="stylesheet" href="stylesheets/app.css">

  <script src="javascripts/modernizr.foundation.js"></script>

  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>
<body>

  <div class="row">
      <div class="twelve columns" id="header">
          <div class="topbar">
              <div class="global-nav">
                  <div class="four columns" id="header-block">
                      <p><a href="http://learnpythonthehardway.org">Learn Python The Hard Way</a></p>
                  </div>
                  <div class="four columns" id="header-block">
                      <p style="color: white"><a href="http://inculcate.me/school/courses/2/">Take The Video Course For $29</a></p>
                  </div>
                  <div class="four columns" id="header-block">
                      <p>
                      <a href="http://ruby.learncodethehardway.org">Ruby</a> |
                      <a href="http://c.learncodethehardway.org">C</a> |
                      <a href="http://sql.learncodethehardway.org">SQL</a> |
                      <a href="http://regex.learncodethehardway.org">Regex</a> |
                      <a href="http://cli.learncodethehardway.org">CLI</a>
                      </p>
                  </div>
              </div>
          </div>
          <h1 class="title">Exercise 42: Is-A, Has-A, Objects, and Classes</h1>
      </div>
  </div>

  <div class="row">
    <div class="eleven columns">
        <p>An important concept that you have to understand is the difference between a
<tt class="docutils literal">Class</tt> and an <tt class="docutils literal">Object</tt>.  The problem is, there is no real &quot;difference&quot; between a
class and an object.  They are actually the same thing at different points in
time.  I will demonstrate by a Zen koan:</p>
<p><tt class="docutils literal">What is the difference between a Fish and a Salmon?</tt></p>
<p>Did that question sort of confuse you? Really sit down and think about it for a
minute.  I mean, a Fish and a Salmon are different but, wait, they are the same
thing right?  A Salmon is a <em>kind</em> of Fish, so I mean it's not different.  But
at the same time, becase a Salmon is a particular <em>type</em> of Fish and so it's actually
different from all other Fish.  That's what makes it a Salmon and not a
Halibut.  So a Salmon and a Fish are the same but different.  Weird.</p>
<p>This question is confusing because most people do not think about real things
this way, but they intuitively understand them.  You do not need to think about
the difference between a Fish and a Salmon because you <em>know</em> how they are
related.  You know a Salmon is a <em>kind</em> of Fish and that there are other kinds of
Fish without having to understand that.</p>
<p>Let's take it one step further, let's say you have a bucket full of 3 Salmon
and because you are a nice person, you have decided to name them Frank, Joe, and
Mary.  Now, think about this question:</p>
<p><tt class="docutils literal">What is the difference between Mary and a Salmon?</tt></p>
<p>Again this is a weird question, but it's a bit easier than the Fish vs.
Salmon question.  You know that Mary is a Salmon, and so she's not really
different.  She's just a specific &quot;instance&quot; of a Salmon.  Joe and Frank are also
instances of Salmon. But, what do I mean when I say instance?  I mean they were
created from some other Salmon and now represent a real thing that has Salmon-like
attributes.</p>
<p>Now for the mind bending idea:  Fish is a <tt class="docutils literal">Class</tt>, and Salmon is a <tt class="docutils literal">Class</tt>, and
Mary is an <tt class="docutils literal">Object</tt>.  Think about that for a second.  Alright let's break it
down real slow and see if you get it.</p>
<p>A Fish is a <tt class="docutils literal">Class</tt>, meaning it's not a <em>real</em> thing, but rather a word we attach
to instances of things with similar attributes.  Got fins? Got gills? Lives in
water?  Alright it's probably a Fish.</p>
<p>Someone with a Ph.D. then comes along and says, &quot;No my young friend, <em>this</em> Fish
is actually <em>Salmo salar</em>, affectionately known as a Salmon.&quot;  This professor
has just clarified the Fish further and made a new <tt class="docutils literal">Class</tt> called &quot;Salmon&quot; that
has more specific attributes.  Longer nose, reddish flesh, big, lives in the ocean or
fresh water, tasty?  Ok, probably a Salmon.</p>
<p>Finally, a cook comes along and tells the Ph.D., &quot;No, you see this Salmon right
here, I'll call her Mary and I'm going to make a tasty fillet out of her with
a nice sauce.&quot;  Now you have this <em>instance</em> of a Salmon (which also is an
instance of a Fish) named Mary turned into something real that is filling your
belly.  It has become an <tt class="docutils literal">Object</tt>.</p>
<p>There you have it: Mary is a kind of Salmon that is a kind of Fish.  <tt class="docutils literal">Object</tt> is
a <tt class="docutils literal">Class</tt> is a <tt class="docutils literal">Class</tt>.</p>
<div class="section" id="how-this-looks-in-code">
<h1>How This Looks In Code</h1>
<p>This is a weird concept, but to be very honest you only have to worry about it
when you make new classes, and when you use a class.  I will show you two
tricks to help you figure out whether something is a <tt class="docutils literal">Class</tt> or
<tt class="docutils literal">Object</tt>.</p>
<p>First, you need to learn two catch phrases &quot;is-a&quot; and &quot;has-a&quot;.  You use the
phrase is-a when you talk about objects and classes being related to each other
by a class relationship.  You use has-a when you talk about objects and classes
that are related only because they <em>reference</em> each other.</p>
<p>Now, go through this piece of code and replace each <tt class="docutils literal"><span class="pre">##??</span></tt> comment with a
replacement comment that says whether the next line represents an <tt class="docutils literal"><span class="pre">is-a</span></tt> or a
<tt class="docutils literal"><span class="pre">has-a</span></tt> relationship, and what that relationship is.  In the beginning of the
code, I've laid out a few examples, so you just have to write the remaining
ones.</p>
<p>Remember, is-a is the relationship between Fish and Salmon, while has-a is the
relationship between Salmon and Gills.</p>
<div class="highlight"><pre><a name="ex--ex42.py-idio.html-1"></a><span class="c">## Animal is-a object (yes, sort of confusing) look at the extra credit</span>
<a name="ex--ex42.py-idio.html-2"></a><span class="k">class</span> <span class="nc">Animal</span><span class="p">(</span><span class="nb">object</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-3"></a>    <span class="k">pass</span>
<a name="ex--ex42.py-idio.html-4"></a>
<a name="ex--ex42.py-idio.html-5"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-6"></a><span class="k">class</span> <span class="nc">Dog</span><span class="p">(</span><span class="n">Animal</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-7"></a>
<a name="ex--ex42.py-idio.html-8"></a>    <span class="k">def</span> <span class="nf">__init__</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">name</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-9"></a>        <span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-10"></a>        <span class="bp">self</span><span class="o">.</span><span class="n">name</span> <span class="o">=</span> <span class="n">name</span>
<a name="ex--ex42.py-idio.html-11"></a>
<a name="ex--ex42.py-idio.html-12"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-13"></a><span class="k">class</span> <span class="nc">Cat</span><span class="p">(</span><span class="n">Animal</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-14"></a>
<a name="ex--ex42.py-idio.html-15"></a>    <span class="k">def</span> <span class="nf">__init__</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">name</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-16"></a>        <span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-17"></a>        <span class="bp">self</span><span class="o">.</span><span class="n">name</span> <span class="o">=</span> <span class="n">name</span>
<a name="ex--ex42.py-idio.html-18"></a>
<a name="ex--ex42.py-idio.html-19"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-20"></a><span class="k">class</span> <span class="nc">Person</span><span class="p">(</span><span class="nb">object</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-21"></a>
<a name="ex--ex42.py-idio.html-22"></a>    <span class="k">def</span> <span class="nf">__init__</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">name</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-23"></a>        <span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-24"></a>        <span class="bp">self</span><span class="o">.</span><span class="n">name</span> <span class="o">=</span> <span class="n">name</span>
<a name="ex--ex42.py-idio.html-25"></a>
<a name="ex--ex42.py-idio.html-26"></a>        <span class="c">## Person has-a pet of some kind</span>
<a name="ex--ex42.py-idio.html-27"></a>        <span class="bp">self</span><span class="o">.</span><span class="n">pet</span> <span class="o">=</span> <span class="bp">None</span>
<a name="ex--ex42.py-idio.html-28"></a>
<a name="ex--ex42.py-idio.html-29"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-30"></a><span class="k">class</span> <span class="nc">Employee</span><span class="p">(</span><span class="n">Person</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-31"></a>
<a name="ex--ex42.py-idio.html-32"></a>    <span class="k">def</span> <span class="nf">__init__</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">name</span><span class="p">,</span> <span class="n">salary</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-33"></a>        <span class="c">## ?? hmm what is this strange magic?</span>
<a name="ex--ex42.py-idio.html-34"></a>        <span class="nb">super</span><span class="p">(</span><span class="n">Employee</span><span class="p">,</span> <span class="bp">self</span><span class="p">)</span><span class="o">.</span><span class="n">__init__</span><span class="p">(</span><span class="n">name</span><span class="p">)</span>
<a name="ex--ex42.py-idio.html-35"></a>        <span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-36"></a>        <span class="bp">self</span><span class="o">.</span><span class="n">salary</span> <span class="o">=</span> <span class="n">salary</span>
<a name="ex--ex42.py-idio.html-37"></a>
<a name="ex--ex42.py-idio.html-38"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-39"></a><span class="k">class</span> <span class="nc">Fish</span><span class="p">(</span><span class="nb">object</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-40"></a>    <span class="k">pass</span>
<a name="ex--ex42.py-idio.html-41"></a>
<a name="ex--ex42.py-idio.html-42"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-43"></a><span class="k">class</span> <span class="nc">Salmon</span><span class="p">(</span><span class="n">Fish</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-44"></a>    <span class="k">pass</span>
<a name="ex--ex42.py-idio.html-45"></a>
<a name="ex--ex42.py-idio.html-46"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-47"></a><span class="k">class</span> <span class="nc">Halibut</span><span class="p">(</span><span class="n">Fish</span><span class="p">):</span>
<a name="ex--ex42.py-idio.html-48"></a>    <span class="k">pass</span>
<a name="ex--ex42.py-idio.html-49"></a>
<a name="ex--ex42.py-idio.html-50"></a>
<a name="ex--ex42.py-idio.html-51"></a><span class="c">## rover is-a Dog</span>
<a name="ex--ex42.py-idio.html-52"></a><span class="n">rover</span> <span class="o">=</span> <span class="n">Dog</span><span class="p">(</span><span class="s">&quot;Rover&quot;</span><span class="p">)</span>
<a name="ex--ex42.py-idio.html-53"></a>
<a name="ex--ex42.py-idio.html-54"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-55"></a><span class="n">satan</span> <span class="o">=</span> <span class="n">Cat</span><span class="p">(</span><span class="s">&quot;Satan&quot;</span><span class="p">)</span>
<a name="ex--ex42.py-idio.html-56"></a>
<a name="ex--ex42.py-idio.html-57"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-58"></a><span class="n">mary</span> <span class="o">=</span> <span class="n">Person</span><span class="p">(</span><span class="s">&quot;Mary&quot;</span><span class="p">)</span>
<a name="ex--ex42.py-idio.html-59"></a>
<a name="ex--ex42.py-idio.html-60"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-61"></a><span class="n">mary</span><span class="o">.</span><span class="n">pet</span> <span class="o">=</span> <span class="n">satan</span>
<a name="ex--ex42.py-idio.html-62"></a>
<a name="ex--ex42.py-idio.html-63"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-64"></a><span class="n">frank</span> <span class="o">=</span> <span class="n">Employee</span><span class="p">(</span><span class="s">&quot;Frank&quot;</span><span class="p">,</span> <span class="mi">120000</span><span class="p">)</span>
<a name="ex--ex42.py-idio.html-65"></a>
<a name="ex--ex42.py-idio.html-66"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-67"></a><span class="n">frank</span><span class="o">.</span><span class="n">pet</span> <span class="o">=</span> <span class="n">rover</span>
<a name="ex--ex42.py-idio.html-68"></a>
<a name="ex--ex42.py-idio.html-69"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-70"></a><span class="n">flipper</span> <span class="o">=</span> <span class="n">Fish</span><span class="p">()</span>
<a name="ex--ex42.py-idio.html-71"></a>
<a name="ex--ex42.py-idio.html-72"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-73"></a><span class="n">crouse</span> <span class="o">=</span> <span class="n">Salmon</span><span class="p">()</span>
<a name="ex--ex42.py-idio.html-74"></a>
<a name="ex--ex42.py-idio.html-75"></a><span class="c">## ??</span>
<a name="ex--ex42.py-idio.html-76"></a><span class="n">harry</span> <span class="o">=</span> <span class="n">Halibut</span><span class="p">()</span>
</pre></div></div>
<div class="section" id="about-class-name-object">
<h1>About class Name(object)</h1>
<p>Remember how I was yelling at you to always use <tt class="docutils literal">class Name(object)</tt> and I couldn't
tell you why?  Now I can tell you, because you just learned about the difference between
a <tt class="docutils literal">class</tt> and an <tt class="docutils literal">object</tt>.  I couldn't tell you until now because you would have
just been confused and couldn't learn to use the technology.</p>
<p>What happened is Python's original rendition of <tt class="docutils literal">class</tt> was broken in many serious
ways.  By the time they admitted the fault it was too late, and they had to support
it.  In order to fix the problem, they needed some &quot;new class&quot; style so that the
&quot;old classes&quot; would keep working but you could use the new more correct version.</p>
<p>This is where &quot;class is-a object&quot; comes in.  They decided that they would use the
word &quot;object&quot;, lowercased, to be the &quot;class&quot; that you inherit from to make a class.
Confusing right?  A class inherits from the class named object to make a class but
it's not an object really it's a class, but do not forget to inherit from object.</p>
<p>Exactly. The choice of one single word meant that I couldn't teach you about this
until now.  Now you can try to understand the concept of a class that is an
object if you like.</p>
<p>However, I would suggest you do not.  Just completely ignore the idea of old style
vs. new style classes and assume that Python always requires (object) when you
make a class.  Save your brain power for something important.</p>
</div>
<div class="section" id="study-drills">
<h1>Study Drills</h1>
<ol class="arabic simple">
<li>Research why Python added this strange <tt class="docutils literal">object</tt> class, and what that means.</li>
<li>Is it possible to use a <tt class="docutils literal">Class</tt> like it's an <tt class="docutils literal">Object</tt>?</li>
<li>Fill out the animals, fish, and people in this exercise with functions that make
them do things.  See what happens when functions are in a &quot;base class&quot; like Animal
vs. in say Dog.</li>
<li>Find other people's code and work out all the is-a and has-a relationships.</li>
<li>Make some new relationships that are lists and dicts so you can also have &quot;has-many&quot;
relationships.</li>
<li>Do you think there's a such thing as a &quot;is-many&quot; relationship?  Read about &quot;multiple
inheritance&quot;, then avoid it if you can.</li>
</ol>
</div>
<div class="section" id="common-student-questions">
<span id="csq"></span><h1>Common Student Questions</h1>
<dl class="docutils">
<dt>What are these <tt class="docutils literal">## <span class="pre">??</span></tt> comments for?</dt>
<dd>Those are &quot;fill-in-the-blank&quot; comments that you are supposed to fill in with the right &quot;is-a&quot;, &quot;has-a&quot;
concepts.  Re-read this exercise and look at the other comments to see what I mean.</dd>
<dt>What is the point of <tt class="docutils literal">self.pet = None</tt>?</dt>
<dd>That makes sure that the <tt class="docutils literal">self.pet</tt> attribute of that class is
set to a default of <tt class="docutils literal">None</tt>.</dd>
<dt>What does <tt class="docutils literal">super(Employee, <span class="pre">self).__init__(name)</span></tt> do?</dt>
<dd>That's how you can run the <tt class="docutils literal">__init__</tt> method of a parent
class reliably.  Go search for &quot;python super&quot; and read the
various advice on it being evil and good for you.</dd>
</dl>
</div>
    </div>

    <div class="one columns" id="right-nav">
        <center>
        <p><a href="/book/"><img src="images/48_structure.png"></a></p>
        <p><a href="mailto:help@learncodethehardway.org"><img src="images/48_email.png"></a></p>
        <p><a href="#csq"><img src="images/48_faq.png"></a></p>
        <p><a href="http://inculcate.me/school/courses/2/"><img src="images/48_video.png"></a></p>
        </center>
    </div>
    <div class="twelve columns" id="footer">
        <div class="four columns" id="footer-block">
            <p>
            <a href="http://learncodethehardway.org">Copyright (C) 2010 Zed. A. Shaw</a>
            </p>
        </div>
        <div class="four columns" id="footer-block">
            <p>
            </p>
        </div>
        <div class="three columns" id="footer-block">
            <p>
            <a href="/credits.html">Credits</a>
            </p>
        </div>
        <div class="one columns">
            &nbsp;
        </div>
    </div>

  <!-- Included JS Files (Compressed) -->
  <script src="javascripts/jquery.js"></script>
  <script src="javascripts/foundation.min.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24168052-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

  </script>

</body>
</html>
